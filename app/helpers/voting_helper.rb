module VotingHelper
	def get_following_month
		months = ["January", "February","March","April","May","June","July","August","September","October","November","December"]
		current_date_month = Time.now.strftime("%m").to_i
		return months[current_date_month]
	end
end
