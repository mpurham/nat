module SuggestionsHelper

  def nerdery_locations
  	locations = {
  		"Bloomington, MN" => "44.829666 -93.315217",
  		"Chicago, IL" => "41.881832 -87.623177",
  		"Phoenix,AZ" => "33.448376 -112.074036"
  	}
  	return locations
  end
end
