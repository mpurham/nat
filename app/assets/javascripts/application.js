// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(function(){

	
	// 	Set the value of suggested snack field
	// 	depending on which option is selected.
	$("#snack_select").on('change', function(){
		var name = this.value;

		if(name != "")
		{
			$("#snack_suggestion").attr("disabled", "disabled");
			$("#snack_suggestion").val(name);
		} 
		else 
		{
			$("#snack_suggestion").prop("disabled", false);
			$("#snack_suggestion").val("");
		}

	});

	var getSnacks = $(".snack_name");
	$("#suggest_snack_btn").attr("disabled","disabled");

  // sets a listener of the snack suggestion 
  $("#snack_suggestion").focusout(function() {
	for(var i=0; i < getSnacks.length; ++i)
	{
		// Does snack entry match a dropdown selection?
		if($("#snack_suggestion").val() == getSnacks[i].textContent  )
		{
			$("#suggest_snack_btn").attr("disabled","disabled");
			$("<p class=\"snack-error\">Snack already exists</p>").appendTo(".common-error").fadeOut(3000);
			break;
		} else {
			$("#suggest_snack_btn").prop("disabled",false);

		}
	}

  });


})
