require "uri"
require "net/http"

class SuggestionsController < ApplicationController
  before_action :current_user
  respond_to :json

  def index
  	@suggestions = Snack.all
  end

  # Send data for new snacks
  def suggest_snack

  	data = {
  		"name": params[:snack_suggestion],
  		"location": params[:suggestion_purchase_location],
  		"latitude": params[:location].split(" ")[0].to_f,
  		"longitude": params[:location].split(" ")[1].to_f
  	}.to_json

    uri = URI.parse('https://api-snacks.nerderylabs.com/v1/snacks/?ApiKey=376b38aa-a42b-49dd-8ff6-0ffababe8b79')
    req = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json'})
    req.body = data

  	respond_to do |format|
      	format.html
      	format.json { render json: req.body, mime_type: Mime::Type.lookup("application/json") }
    end

	rescue => e
    	puts "failed #{e}"
  end

  def upvote
    if(current_user.emp_vote_count.to_i > 2 && current_user.emp_vote_month.present?)
      flash[:error] = "You have reached max votes for #{Date.today.strftime("%B")}. Try voting again next month."
      redirect_to voting_index_path
    else
      snack_name =  /\"(.*)\"/.match( params[:up] )[0].gsub("\"","")
      @curr_snack = Snack.find_by(name: snack_name)
      current_user.vote(@curr_snack, :up)
      Employee.employee_vote_counter(current_user.empUID)
      flash[:notice] = "+1, thanks for voting!"
      redirect_to voting_index_path
    end
  end

end
