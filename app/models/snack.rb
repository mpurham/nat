class Snack
  require 'net/http'
  require 'json'
 
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongo::Voteable
  include ActiveModel::Validations

  field :name, type: String
  field :optional, type: Mongoid::Boolean
  field :purchased_locations, type: String
  field :purchased_count, type: Integer
  field :last_purchased_date, type: String
  field :uid, type: Integer

  scope :current_purchases, lambda{where(optional: false)}
  scope :not_purchased, lambda{where(purchased_count: 0)}

  validates_presence_of :purchased_locations

  # set points for each vote
  voteable self, :up => +1, :down => -1

  # Checks to see if record exists before
  # inserting into the database
  def self.ignore_duplicates(suid)
  	return true if(self.find_by(uid: suid).present?)
  	rescue Mongoid::Errors::DocumentNotFound  => e
  	return false
  end

  # Fetches json data and stores it as
  # a collection in DB. Avoids calling
  # api during each request
  def self.update_snacks
	url = 'https://api-snacks.nerderylabs.com/v1/snacks/?ApiKey=376b38aa-a42b-49dd-8ff6-0ffababe8b79'
	uri = URI(url)
	response = Net::HTTP.get(uri)

	sample_data = JSON.parse(response)

 	(0..sample_data.size-1).each do |snk|

 		if( !(ignore_duplicates(sample_data[snk]["id"]) ) )

	 		snack_obj = self.new
	 		snack_obj.name = sample_data[snk]["name"]
	 		snack_obj.optional = sample_data[snk]["optional"]
	 		snack_obj.purchased_locations = sample_data[snk]["purchaseLocations"] 
	 		snack_obj.purchased_count = sample_data[snk]["purchaseCount"]
	 		snack_obj.last_purchased_date = sample_data[snk]["lastPurchaseDate"]
	 		snack_obj.uid = sample_data[snk]["id"]

			snack_obj.save!
		end

		puts "record exists"

	end
  end 

end
