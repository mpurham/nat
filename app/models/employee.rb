class Employee
  include Mongoid::Document
  include Mongo::Voter

  field :first_name, type: String
  field :last_name, type: String
  field :empUID, type: String
  field :emp_vote_count, type: Integer, default: 0
  field :emp_vote_month, type: String

	# Public: Increment Vote Count
	#
	# uid  - Current user id
  def self.employee_vote_counter(uid)
  	current_month = Date.today.strftime("%B")
  	emp = self.find_by(empUID: uid)
  	if(!emp.emp_vote_month.present?)
    	emp.emp_vote_month = current_month 
    end
    emp.emp_vote_count += 1
    emp.save!
  end
end
