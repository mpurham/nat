json.extract! snack, :id, :name, :optional, :purchased_locations, :purchased_count, :last_purchased_date, :uid, :created_at, :updated_at
json.url snack_url(snack, format: :json)
