Rails.application.routes.draw do
  resources :suggestions, only: [:index]

  post 'suggestions/suggest-snacks' => 'suggestions#suggest_snack'
  post 'suggestions/up' => 'suggestions#upvote'
  post 'suggestions/down' => 'suggestions#downvote'

  resources :snacks
  resources :voting, only: [:index]
  resources :employees

  root 'suggestions#index'
end
