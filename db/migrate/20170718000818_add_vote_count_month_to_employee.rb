class AddVoteCountMonthToEmployee < ActiveRecord::Migration
  def self.up
    add_column :employees, :emp_vote_count, :integer
    add_column :employees, :emp_vote_month, :string
  end

  def self.down
  	remove_column :employees, :emp_vote_count
  	remove_column :employees, :emp_vote_month
  end
end
