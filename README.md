# Development Versions

- Rails v4.2.8 (without active record, only needed modules)
- Ruby v2.2.6
- MongoDB v3.4.4 

# Application pages

Voting (http://localhost:3000/voting):

Suggestions (http://localhost:3000/suggestions/):

Employee (http://localhost:3000/employees ): Used for creating a temp user for using the application

Snacks (http://localhost:3000/snacks): Allows you to sync snacks locally to avoid making numerous api calls

To view all routes locally, simply type `rake routes` to view all routes for the application.

# Pre start

Before installing and running this application you will need to install mongodb via your local machine. If you are using Mac OS then you may install mongodb using homebrew.

`brew install mongodb`

# Getting started

Download and install application via the git repository. 

`git clone git@bitbucket.org:mpurham/nat.git`

Now cd into the clone directory.

`cd nat`

Once you are inside of the nat folder we will create our mongodb database. Run 

`brew services start mongodb`

The above command starts a background service, which essentially runs the database without a gui. Now lets start the mongodb shell.

`mongo`

Once the interactive shell has started, run `use [YOUR_DATABASE_NAME]` to create a local database. Now run `show dbs` to see if your newly created db appears.

Assuming all has worked well thus far, we will begin install our rails application. Inside of the cloned rails directory, run:

`bundle install`

The above command will download all necessary dependencies for this project. 

# Running the application

Now we are ready to run the rails application. Run:

`rails s`

Upon loading the application, select the `create an account` button via the link located on the top right side of the page. After you have created a temp employee account, you will need to sync the snacks from the api.

Simply go to `http://localhost:3000/snacks` and select the `Refresh Snacks` button. The refresh snacks button allows you to sync snacks locally to avoid making numerous api calls


After you have succesfully done the above, you may begin using the application.

